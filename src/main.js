import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import http from './services/http.js'

import iView from 'iview';
import 'iview/dist/styles/iview.css';

import routes from './routes.js';

Vue.prototype.$http = http

Vue.use(VueRouter)
Vue.use(iView)

// Import Helpers for filters
import { domain, count, prettyDate, pluralize } from './filters/index.js'

// Import Install and register helper items
Vue.filter('count', count)
Vue.filter('domain', domain)
Vue.filter('prettyDate', prettyDate)
Vue.filter('pluralize', pluralize)

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: routes
})

import Auth from './services/auth.js';

router.beforeEach((to, from, next) => {
    if(to.meta.requireAuth && !Auth.authenticated())
    {
      next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
    }
    else {
      next()
    }
})

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
});

export default router
